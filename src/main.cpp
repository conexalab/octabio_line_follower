#include <Octabio.h>
#include <Motor.h>

void setup() {

  // Se configuran las entradas para los sensores infrarojos.
  pinMode(PIN_A, INPUT); // Izquierda
  pinMode(PIN_B, INPUT); // Derecha

  // Funciones auxiliares para configurar los puertos para los motores.
  setUpMotor(2); // Izquierda
  setUpMotor(1); // Derecha
}

void loop() {  
  // Se leen los sensores infrarojos.
  
  bool isLeftFree = analogRead(PIN_A)>500; // 1 Libre, 0 Detectado
  bool isRightFree = analogRead(PIN_B)>500; // 1 Libre, 0 Detectado
  
  if(!isLeftFree && isRightFree){
    setMotor(1, -255); // Se mueve el motor izquierdo.
    setMotor(2, 0);
  }

  if(isLeftFree && !isRightFree){
    setMotor(1, 0);
    setMotor(2, 255); // Se mueve el motor derecho.
  }

  if(isLeftFree && isRightFree){
    setMotor(1, 0);
    setMotor(2, 0);
  }

  if(!isLeftFree && !isRightFree){
    // Ambos motores se encenden
    setMotor(1, -255);
    setMotor(2, 255);
  }

}