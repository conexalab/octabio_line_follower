#ifndef MOTOR
#define MOTOR

#include "Arduino.h"

void setUpMotor(int motor);

void motores(int pin_a, int pin_b, int velocidad);

void setMotor(int motor, int velocidad);

#endif