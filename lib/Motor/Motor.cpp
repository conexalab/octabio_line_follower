#include "Motor.h"

void motores(int pin_a, int pin_b, int velocidad){
    if(velocidad>0){
        analogWrite(pin_a, velocidad);
        digitalWrite(pin_b, 0);
    }

    if(velocidad<0){
        analogWrite(pin_b, (-1)*velocidad);
        digitalWrite(pin_a, 0);
    }

    if(velocidad==0){
        digitalWrite(pin_a, 0);
        digitalWrite(pin_b, 0);
    }
}

void setMotor(int motor, int velocidad){
    // Entrada D
    if(motor==1){
        // Pines de la entrada C
        motores(5, 6, velocidad);
    }

    // Entrada C
    if(motor==2){
        // Pines de la entrada D
        motores(9, 10, velocidad);
    }
}

void setUpMotor(int motor){
      // Entrada D
    if(motor==1){
        // Pines de la entrada C
        pinMode(5, OUTPUT);
        pinMode(6, OUTPUT);
    }

    // Entrada C
    if(motor==2){
        // Pines de la entrada D
        // Pines de la entrada C
        pinMode(9, OUTPUT);
        pinMode(10, OUTPUT);
    }
}